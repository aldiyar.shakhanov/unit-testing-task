import globals from 'globals';
import pluginJs from '@eslint/js';
import google from 'eslint-config-google';
import prettier from 'eslint-config-prettier';
import prettierPlugin from 'eslint-plugin-prettier';

const googleConfig = {
  ...google,
  rules: {
    ...google.rules,
    'valid-jsdoc': 'off',
    'require-jsdoc': 'off',
    'linebreak-style': ['error', 'unix'], // Use Unix-style line endings
    'prettier/prettier': ['error', { singleQuote: true, endOfLine: 'lf' }],
  },
};

export default [
  {
    ignores: ['node_modules', 'coverage', 'mochawesome-report'],
  },
  {
    languageOptions: {
      globals: { ...globals.node, ...globals.mocha },
    },
  },
  pluginJs.configs.recommended,
  googleConfig,
  prettier,
  {
    plugins: {
      prettier: prettierPlugin,
    },
    rules: {
      'prettier/prettier': 'error',
    },
  },
];
