import NumbersValidator from '../../app/numbers_validator.js';
import { expect } from 'chai';

describe('isAllNumbers positive test', () => {
  let validator;
  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('should return true when provided with an array of numbers', () => {
    const validationResults = validator.isAllNumbers([1, 2, 3, 4, 5]);
    expect(validationResults).to.be.equal(true);
  });

  it('should return false when it is not provided with an array of numbers', () => {
    const validationResults = validator.isAllNumbers([1, 'str', 3, 4, 5]);
    expect(validationResults).to.be.equal(false);
  });

  it('should throw an error when provided an object', () => {
    expect(() => {
      validator.isAllNumbers({ k: '1' });
    }).to.throw('[[object Object]] is not an array');
  });
});
