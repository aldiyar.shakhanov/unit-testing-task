import NumbersValidator from '../../app/numbers_validator.js';
import { expect } from 'chai';

describe('getEvenNumbersFromArray positive test', () => {
  let validator;
  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('should return array of even numbers', () => {
    const validationResults = validator.getEvenNumbersFromArray([
      1, 2, 3, 4, 5, 6, 7, 8,
    ]);
    expect(validationResults).to.deep.equal([2, 4, 6, 8]);
  });

  it('should return array of all numbers', () => {
    const validationResults = validator.getEvenNumbersFromArray([
      1, 3, 5, 7, 9,
    ]);
    expect(validationResults).to.deep.equal([]);
  });

  it('should throw an error when provided an array with non-number elements', () => {
    expect(() => {
      validator.getEvenNumbersFromArray([1, 2, 3, 4, 'five']);
    }).to.throw('[1,2,3,4,five] is not an array of "Numbers"');
  });
});
